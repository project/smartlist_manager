This module provides an interface through which users can subscribe to mailing lists run by procmail/smartlist

My shared hosting provider did not permit the use of mailman, but offers the widly used mailing list system 
smartlist which runs on top of procmail.
It is by far not as user friendly as mailman, but if you do not have the option, then you have to work 
with what you have, right?

If you are in the same situation, and do not know if your provider offers smartlist or not, then here is a hint.
If you created a email list (e.g. examplelist@domain.com) in your providers web panel, and you have to append "-request" 
to the name (examplelist-request@domain.com) to trigger an action like subscribing/unsubscribing, 
then you almost certainly work with smartlist.

I used the mailman_manager codebase as a starting point and modified it to work with smartlist.
Smartlist_manager works independent from mailman_manager but I suggest to use only one or the other,
but not both modules at the same time.

The administer page allows administrators to add or delete lists and program appropriate request addresses to which 
commands are sent by Drupal via email.

Other than the original confirmation of subscription by the user, the use of smartlist as a mailing list software
is more or less invisible to the enduser as all settings can be changed from within Drupal.

The combination of this module with mailhandler and listhandler modules replicates to a great extent the
functionality of Yahoo!Groups or Google Groups.

This module works with the help of smartlists "X-Command" remote commands. That allows to create lists only
maintained by drupal. (You can even disable "automatic subsciption" and "automatic unsubscription").

Here the instructions on how to make your smartlist mailing list work with smartlist_manager.
Just assume you like to setup a mailing list "examplelist@domain.com" with user "admin@domain.com" as owner of the list.

1) Create the list "examplelist" in your providers web interface with "admin" as owner. 
   Set the options "automatic subsciption" and "automatic subsciption" depending on your needs.
2) Use ssh/telnet to your webspace and locate the directory where the mailing list resides.
   In my case it is located under ~/mail/examplelist.
3) open file rc.custom and add the line

X_COMMAND_PASSWORD = mypassword

mypassword is the password that will be used for the command processing.

Now is the time to set up the mailing list in smartlist_manager:
4) enable the module in "Administer"/"Site building"/"Modules".
5) create a new mailing list: goto "Administer"/"Site Configuration"/"Smartlist mailing lists"/"Add new mailing list".
6) Set the following options:
Mailing List name: examplelist@domain.com
Mailing List 'admin' address: admin@domain.com
X-Command password: mypassword
Roles: Select the roles that you want to allow to subscribe to the list.

Thats it.

7) Now for each user who wants to subscribe or unsubscribe , go to "my account"/"edit"/"mailing lists"
   and select "Subscribe" or "Implemented Unsubscribe".

   
--------------------------------------------------------
Implemented by Ulf Schenk.
If you like to support my work on RSVP and smartlist_mailman for Drupal 6,
please consider donating to my PayPal account. The link can be found on the project page
http://drupal.org/project/smartlist_manager
