<?php

/**
 * @file
 * Allows users to subscribe to Smartlist mailing lists via a form in
 * their user profile.  List of mailing lists is defined by administrator.
 * Module maintains a list of user subscriptions and passwords.
 * Module sends requests for subscription changes to Smartlist request address.
 */

/**
 * Implementation of hook_help().
 */
function smartlist_manager_help($path, $arg) {
  switch ($path) {
    case 'admin/modules#description':
      return t('Utilities related to Smartlist manager, subscription system and database for Smartlist mailing lists.');
    case 'admin/help#smartlist_manager':
      $output = '<p>'. t('Utilities related to Smartlist manager, subscription system and database for Smartlist mailing lists.') .'</p>';
      $output .= '<p>'. t("This tool works using the smartlist email interface. All commands sent by this module are also BCC'ed to the list admin. One can optionally specify the web interface and/or the web archive of each list. The module can not know if a user is previously subscribed but allows the user to specify a password which can then be used to hook into an existing account.") .'</p>';
      return $output;
  }
}

/**
 * Implementation of hook_link().
 */
function smartlist_manager_link($type, $node = NULL, $teaser = FALSE) {
  $links = array();

  if ($type == 'page' && user_access('access content')) {
    // $links[] = l(t('mailing %list', array('%list' => format_plural(_smartlist_manager_get_count(), 'list', 'lists'))), 'smartlist_manager', array('title' => t('Subscribe to mailing %list', array('%list' => format_plural(_smartlist_manager_get_count(), 'list', 'lists')))));
    $links[] = l(t('Mailing Lists'), 'smartlist_manager', array('title' => t('Subscribe to mailing lists')));
  }

  return $links;
}

/**
 * Implementation of hook_perm().
 */
function smartlist_manager_perm() {
  return array('access smartlist_manager', 'administer smartlist_manager');
}

/**
 * Implementation of hook_menu().
 */
function smartlist_manager_menu() {
  $items = array();
/*
  $items['smartlist_manager'] = array(
    'title' => 'Mailing lists',
    'page callback' => 'smartlist_manager_page',
    'access arguments' => array('access smartlist_manager'),
    'type' => MENU_NORMAL_ITEM,
  );
*/
  $items['admin/settings/smartlist_manager'] = array(
    'title' => t('Smartlist mailing lists'),
    'access arguments' => array('administer smartlist_manager'),
    'description' => t('Allow users to subscribe and change their subscriptions to Smartlist based mailing lists.'),
    'page callback' => '_smartlist_manager_admin_display',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/settings/smartlist_manager/add'] = array(
    'title' => 'Add New',
    'access arguments' => array('administer smartlist_manager'),
    'description' => 'Add new Smartlist mailing lists.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('smartlist_manager_admin_form', NULL),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/settings/smartlist_manager/edit/%'] = array(
    'title' => 'Edit Mailing list',
    'access arguments' => array('administer smartlist_manager'),
    'description' => 'Edit Smartlist mailing lists.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('smartlist_manager_admin_form', 4),
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/smartlist_manager/delete/%'] = array(
    'title' => 'Delete Mailing list',
    'access arguments' => array('administer smartlist_manager'),
    'description' => 'Add new Smartlist mailing lists.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('smartlist_manager_admin_delete_form', 4),
    'type' => MENU_CALLBACK,
  );
  $items['user/%user/edit/smartlist_manager'] = array(
    'title' => 'Mailing Lists',
    'access callback' => '_smartlist_manager_access',
    'access arguments' => array(1),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_smartlist_manager_user_form', 1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 9,
  );
  return $items;
}

/**
 * Menu callback; Forwards request to user subscription form;
 */
function smartlist_manager_page() {
  if (!user_access('access smartlist_manager')) {
    return t('You must be an authorized member to subscribe to mailing lists');
  }
  else {
    global $user;
    drupal_goto('user/'. $user->uid .'/edit/smartlist_manager');
  }
}

function _smartlist_manager_user_form(&$form_state, $account) {
  $form = array();
  $output = '<div class="smartlist_manager">';
  $output .= '<p>'. t('The following email address will be used for subscriptions to all email lists: ') .'<strong>'. $account->mail .'</strong><br />';
  $output .= t('If you would like to have a different email address subscribed, change your email address in the account settings tab.');
  $lists = _smartlist_manager_get_lists();
  global $user;
  
  if (count($lists) == 0) {
    $output = t('There are no lists available for subscription.');
  }
  else {
    $status = 0;
    foreach ($lists as $list) {
      if (_smartlist_manager_roles_access($account, $list['roles']) || ($user->uid == 1)) {
      
        $subscrip = _smartlist_manager_get_subscriptions($account->uid, $list['lid']);
        $form['list'. trim($list['lid'])] = array(
          '#type' => 'fieldset',
          '#title' => $list['name'],
          '#collapsible' => TRUE
        );

        if ($subscrip['lstatus'] == 0) {
          $options = array(
          //US          '2' => t('Subscribe for digest (receive emails in a bundle)'),
            '3' => t('Subscribe for all Mail (normal delivery)')
          );

          $status = '<p>'. t('Your current email address is not subscribed to list %list.', array('%list' => $list['name'])) .'</p>';
        }
        else {
          $options = array(
            '0' => t('Unsubscribe'),
          //US          '1' => t('No mail (temporarily disable delivery)'),
          //US          '2' => t('Subscribe for digest (receive emails in a bundle)'),
          //US          '3' => t('Subscribe for all Mail (normal delivery)'),
          //US          '4' => t('Mail password')
          );

          $status = '<p>'. t('You are subscribed to mailing list %list_name', array('%list_name' => $list['name'])) .'</p>';
        }

        $form['list'. trim($list['lid'])]['mman_info'] = array(
          '#type' => 'markup',
          '#value' => $status,
          '#weight' => -1,
        );
        
        /*US Aplus mailinglist does not have a password
         //Prevent to change subscription state when there is no password to manage it
         if ($subscrip['lstatus'] > 0 && empty($subscrip['lpass'])) {
         $form['list'. trim($list['lid'])]['msg_disabled'. trim($list['lid'])] = array(
         '#value' => t('Sorry, you are not allowed to change this list subscription.'),
         );
  
         $options = array(
         $subscrip['lstatus'] => $options[$subscrip['lstatus']],
         '4' => t('Mail password'),
         );
         }
         */
        $form['list'. trim($list['lid'])]['options'. trim($list['lid'])] = array(
          '#type' => 'radios',
          '#title' => t('Change your subscription'),
          '#options' => $options,
          '#default_value' => $subscrip['lstatus'],
        );
        /*US APlus mailinglist does not have a password
         if ($subscrip['lstatus'] == 0) {
         $form['list'. trim($list['lid'])]['pass'. trim($list['lid'])] = array(
         '#type' => 'textfield',
         '#title' => t('Password for %listname (optional)', array('%listname' => $list['name'])),
         '#size' => 15,
         '#required' => FALSE,
         '#default_value' => t($subscrip['lpass'])
         );
         }
         */

        if ($list['web'] || $list['webarch']) {
          $link_output = '<p>'. t('Visit') .': ';
          if ($list['web']) {
            $link_output .= l(t('Smartlist Interface'), $list['web']);
          }
          if ($list['web'] && $list['webarch']) {
            $link_output .= ' '. t('or') .': ';
          }
          if ($list['webarch']) {
            $link_output .= l($list['name'] .' '. t('archive') .'.', $list['webarch']);
          }
          $form['list'. trim($list['lid'])]['links'] = array('#type' => 'markup', '#value' => $link_output);
        }
      }
    }
    
    $output .= '</div>';
    $form['lists']['oldemail'] = array(
      '#type' => 'hidden',
      '#default_value' => $subscrip['lmail'],
    );
    $form['lists']['newemail'] = array(
      '#type' => 'hidden',
      '#default_value' => $account->mail,
    );
  }
  $form['mman_info'] = array(
    '#type' => 'markup',
    '#value' => $output,
    '#weight' => -1,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  $form_state['smartlistm_account'] = $account;
  return $form;
}

/**
 * Checks whether an email address is subscribed to the mailinglist
 * when a new user signs up. If so, changes uid from 0 to the new uid
 * in sn_subscriptions so that the user's subscription status is known
 * when he logs in.
 */
function _smartlist_manager_user_form_submit($form, &$form_state) {
  $account = $form_state['smartlistm_account'];
  if (!is_numeric($account->uid)) {
    return FALSE;
  }
  $lists = _smartlist_manager_get_lists();
  foreach ($lists as $list) {
    $subscrip = _smartlist_manager_get_subscriptions($account->uid, $list['lid']);
    $listno = 'options'. trim($list['lid']);
    $query = 'SELECT * FROM {smartlist_users} WHERE uid = %d AND lid = %d';
    $result = db_query($query, $account->uid, $list['lid']);
    $subscrip = db_fetch_array($result);
    /*US
     if ($form_state['values']['pass'. trim($list['lid'])] == '') {
     $password = $subscrip['lpass'];
     }
     else {
     $password = $form_state['values']['pass'. trim($list['lid'])];
     }
     */

    if ($subscrip['lstatus'] != $form_state['values'][$listno]) {
      $result = _smartlist_manager_update_subscriptions($account->uid, $list['lid'], $form_state['values'][$listno],
                $subscrip['lstatus'], $account->mail);

      if ($result != false)
        drupal_set_message(t('Your mailing list subscription for %list has been updated', array('%list' => $list['name'])));
    }
  }
}

function smartlist_manager_admin_delete_form(&$form_state, $lid) {
  $form = array();
  $list = smartlist_manager_get_list($lid);
  $form['lid'] = array(
    '#type' => 'hidden',
    '#value' => $lid,
  );
  $form['message'] = array('#type' => 'markup', '#value' => t('Are you sure?'));
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete !listname', array('!listname' => $list->name)),
  );
  return $form;
}

function smartlist_manager_admin_delete_form_submit($form, &$form_state) {
  if (!user_access('administer smartlist_manager')) {
    return '';
  }
  $result = db_query('DELETE FROM {smartlist_users} WHERE lid = %d', $form_state['values']['lid']);

  if ($result && db_query('DELETE FROM {smartlist_lists} WHERE lid = %d', $form_state['values']['lid'])) {
    watchdog('smartlist man', 'Successfully deleted Smartlist list %listid', array('%listid' => $list['lid']), WATCHDOG_NOTICE);
    drupal_goto('admin/settings/smartlist_manager');
  }
  else {
    watchdog('smartlist man', 'Smartlist list %listid deletion failed', array('%listid' => $list['lid']), WATCHDOG_ERROR);
    drupal_set_message('Error deleting Mailing list', 'error');
  }
}

function smartlist_manager_get_list($id) {
  return db_fetch_object(db_query('SELECT * FROM {smartlist_lists} WHERE lid = %d', $id));
}


function smartlist_manager_admin_form(&$form_state, $lid = NULL) {
  $form = array();
  if ($lid) {
    $list = smartlist_manager_get_list($lid);
    $form['lid'] = array(
      '#type' => 'hidden',
      '#value' => $lid,
    );
  }

  $form['name'] = array(
    '#title' => 'Mailing List name',
    '#default_value' => !empty($list->name) ? $list->name : '',
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('Fully quallyfied email adress of the list (e.g. listname@domain.com)'),
  );
  $form['adminaddress'] = array(
    '#title' => t("Mailing List 'admin' address"),
    '#default_value' => !empty($list->admin) ? $list->admin : '',
    '#description' => t('Admin email address for the list'),
    '#type' => 'textfield',
  );
  $form['password'] = array(
    '#title' => t("X-Command password"),
    '#default_value' => !empty($list->password) ? $list->password : 'password',
    '#description' => t('The X-Command password you set for the mailing list. Default is \'password\'.'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['bcc'] = array(
    '#title' => t("BCC address"),
    '#default_value' => !empty($list->bcc) ? $list->bcc : '',
    '#description' => t('(Optional. Address is used for BCC)'),
    '#type' => 'textfield',
  );
  /*
   $form['webaddress'] = array(
   '#title' => t('Mailing list web address for users'),
   '#default_value' => !empty($list->web) ? $list->web : '',
   '#description' => t('http://...' ) . t('Leave empty if hidden.'),
   '#type' => 'textfield',
   );
   $form['webarchive'] = array(
   '#title' => t('Mailing list web archive address for users'),
   '#default_value' => !empty($list->webarch) ? $list->webarch : '',
   '#description' => t('http://.... ') . t('Leave empty if hidden.'),
   '#type' => 'textfield',
   );
   */

  $role_query_result = db_query("SELECT rid,name FROM {role} order by name");
  $role_options = array();
  while ($role = db_fetch_object($role_query_result)) {
    $role_options[$role->rid] = $role->name;
  }

  $form['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#options' => $role_options,
    '#description' => t('Select which roles have permissions to subscribe to this mailing list.'),
    '#default_value' => explode(';', $list->roles),
  );
  
  $form['Submit'] = array(
    '#type' => 'submit',
    '#value' => ($lid ? t('Save') : t('Add')),
  );

  return $form;
}

function smartlist_manager_admin_form_validate($form, &$form_state) {
  // Check name is unique
  $lid = !empty($form_state['values']['lid']) ? $form_state['values']['lid'] : 0;
  $result = db_query("SELECT COUNT(*) from {smartlist_lists} WHERE name = '%s'". ($lid ? ' AND lid <> %d' : ''), $form_state['values']['name'], $lid);
  $num_rows = db_result($result);
  if ($num_rows != '0') {
    form_set_error('name', 'Name already in use');
  }
  if ($form_state['values']['adminaddress'] && !valid_email_address($form_state['values']['adminaddress'])) {
    form_set_error('adminaddress', t('"%adminaddress" is not a valid email address', array('%adminaddress' => $form_state['values']['adminaddress'])));
  }
  if ($form_state['values']['bcc'] && !valid_email_address($form_state['values']['bcc'])) {
    form_set_error('bcc', t('"%bcc" is not a valid email address', array('%bcc' => $form_state['values']['bcc'])));
  }
  /*
   if ($form_state['values']['webaddress'] && !valid_url($form_state['values']['webaddress'])) {
   form_set_error('webaddress', t('"%webaddress" is not a valid url', array('%webaddress' => $form_state['values']['webaddress'])));
   }
   if ($form_state['values']['webarchive'] && !valid_url($form_state['values']['webarchive'])) {
   form_set_error('webarchive', t('"%webarchive" is not a valid url', array('%webarchive' => $form_state['values']['webarchive'])));
   }
   */
}

/**
 * Save new mailing list.
 */
function smartlist_manager_admin_form_submit($form, &$form_state) {
  if (!user_access('administer smartlist_manager')) {
    return '';
  }
  
  //array_diff removes all '0' values from the returned selection 
  $roles = implode(";", array_diff($form_state['values']['roles'], array('0')));
  
  if (!empty($form_state['values']['lid'])) {
    // Update existing

    $query = "UPDATE {smartlist_lists} SET name = '%s', password = '%s', admin = '%s', bcc = '%s', roles = '%s' WHERE lid = %d";
    $result = db_query($query,
    $form_state['values']['name'],
    $form_state['values']['password'],
    $form_state['values']['adminaddress'],
    $form_state['values']['bcc'],
    $roles,
    //      $form_state['values']['webaddress'],
    //      $form_state['values']['webarchive'],
    $form_state['values']['lid']);
    if ($result) {
      $form_state['redirect'] = 'admin/settings/smartlist_manager';
    }
  }
  else {
    // Create new
    //    $query = "INSERT INTO {smartlist_lists} (name, command, admin, web, webarch) VALUES ('%s', '%s', '%s', '%s', '%s')";
    $query = "INSERT INTO {smartlist_lists} (name, password, admin, bcc, roles) VALUES ('%s', '%s', '%s', '%s', '%s')";
    $result = db_query($query,
    $form_state['values']['name'],
    $form_state['values']['password'],
    $form_state['values']['adminaddress'],
    $form_state['values']['bcc'],
    roles
    );
    //      $form_state['values']['webaddress'],
    //      $form_state['values']['webarchive'])
    if ($result) {
      $message = 'New Smartlist list %name successfully created';
      watchdog('smartlist man', $message, array('%name' => $name), WATCHDOG_NOTICE);
      $form_state['redirect'] = 'admin/settings/smartlist_manager';
    }
    else {
      $message = t('Error in creating new Smartlist list %name', array('%name' => $name));
      watchdog('smartlist man', $message, WATCHDOG_ERROR);
      return $message;
    }
  }
}

/**
 * Admin display of smartlist lists.
 */
function _smartlist_manager_admin_display() {
  if (!user_access('administer smartlist_manager')) {
    return '';
  }

  $headers = array( array('data' => t('ID'), 'field' => 'lid', 'sort' => 'asc'),
                    array('data' => t('Name'), 'field' => 'name'),
  //                array('data' => t('Request Address'), 'field' => 'command'),
                    array('data' => t('Actions')),
  );

  $query = 'SELECT lid, name FROM {smartlist_lists} '. tablesort_sql($headers);

  $num_per_page = 15;
  $result = pager_query($query, $num_per_page);
  $lists = array();
  while ($list = db_fetch_array($result)) {
    $list['actions'] = l(t('Edit'), 'admin/settings/smartlist_manager/edit/'. $list['lid']) .'<br />'. l(t('Delete'), 'admin/settings/smartlist_manager/delete/'. $list['lid']);
    $lists[] = $list;
  }

  $output = theme('table', $headers, $lists) . theme('pager', array(), $num_per_page);
  $output .= l(t('Add new mailing list'), 'admin/settings/smartlist_manager/add');

  return $output;
}

/**
 * Return array of objects of current mailing lists.
 */
function _smartlist_manager_get_lists() {
  $result = db_query('SELECT * FROM {smartlist_lists} ORDER BY name');
  $lists = array();
  while ($list = db_fetch_array($result)) {
    $lists[] = $list;
  }
  return $lists;
}

/**
 * Return array of user's subscriptions to mailing lists.
 */
function _smartlist_manager_get_subscriptions($uid, $lid) {
  $subscription = db_fetch_array(db_query('SELECT * FROM {smartlist_users} WHERE uid = %d AND lid = %d', $uid, $lid));
  // If there are no entries for this user then set subscriptions to zero
  // and return no mail button option for display
  if (empty($subscription)) {
    db_query('INSERT INTO {smartlist_users} (uid, lid, lstatus) VALUES (%d, %d, %d)', $uid, $lid, 0);
    $subscription = array(
      'uid' => $uid,
      'lid' => $lid,
      'lstatus' => 0,
    );
  }

  return $subscription;
}

/**
 * Update user's subscriptions to mailing lists.
 */
function _smartlist_manager_update_subscriptions($uid, $lid, $lstatus, $oldstatus, $mail_subscribe) {

  $result = false;

  switch ($lstatus) {
    // Unsubscribe selected;
    case 0:
      //US      $command = 'unsubscribe '. $password .' address='. $mail;
      $command = 'unsubscribe ' . $mail_subscribe;
      $x_command = ' unsubscribe '. trim($mail_subscribe);

      $result = _smartlist_manager_setdelivery($uid, $lid, $mail_subscribe, $command, $x_command);
      watchdog('smartlist man', 'User %uid unsubscribed from list %lid. Err=%err', array('%err' => $result, '%lid' => $lid, '%uid' => $uid), WATCHDOG_NOTICE);
      break;
      /*US APlus does not support digest
       // No email selected;
       case 1:
       $command = 'set authenticate '. $password .' address='. $mail ."\n";
       $command .= 'set delivery off';
       _smartlist_manager_setdelivery($uid, $lid, $mail_subscribe, $mail_admin, $command, $x_command);
       watchdog('smartlist man', 'Subscription to list %lid for user %uid changed to no mail', array('%lid' => $lid, '%uid' => $uid), WATCHDOG_NOTICE);
       break;
       */

      /*US APlus does not support digest
       //Digest selected;
       case 2:
       if ($oldstatus == 0) {
       _smartlist_manager_subscribe($uid, $lid, $mail_subscribe, 'digest');
       }
       else {
       $command = 'set authenticate '. $password .' address='. $mail_subscribe ."\n";
       $command .= "set delivery on\n";
       $command .= 'set digest plain';
       _smartlist_manager_setdelivery($uid, $lid, $mail_subscribe, $mail_admin, $command, $x_command);
       watchdog('smartlist man', 'Subscription to list %lid for user %uid changed to digest', array('%lid' => $lid, '%uid' => $uid), WATCHDOG_NOTICE);
       }
       break;
       */
      //All mail selected;
    case 3:
      //US      if ($oldstatus == 0) {
      $result = _smartlist_manager_subscribe($uid, $lid, $mail_subscribe, 'nodigest');
      //US      }
      //US      else {
      //US        $command = 'set authenticate '. $password .' address='. $mail_subscribe ."\n";
      //US        $command .= "set delivery on\n";
      //US        $command .= 'set digest off';
      //US        _smartlist_manager_setdelivery($uid, $lid, $mail_subscribe, $mail_admin, $command, $x_command);
      //US        watchdog('smartlist man', 'Subscription to list %lid for user %uid changed to all mail', array('%lid' => $lid, '%uid' => $uid), WATCHDOG_NOTICE);
      //US      }
      break;
      /*US APlus does not support digest

      //mail pwd selected;
      case 4:
      if ($oldstatus == 0) {
      return;
      }
      else {
      $command = 'address='. $mail_subscribe ."\n";
      $x_command = $mail_admin . ' ' . $password . ' subscribe '. trim($mail_subscribe);
      //US
      _smartlist_manager_setdelivery($uid, $lid, $mail_subscribe, $mail_admin, $command, $x_command);
      watchdog('smartlist man', 'Password for list %lid for user %uid sent by mail', array('%lid' => $lid, '%uid' => $uid), WATCHDOG_NOTICE);
      }
      break;
      */
  
  }
  
  // Do not update status for 'mail password', or if mailing was unsucessful:
  if ($lstatus != 4 && $result == true) {
    $result = db_query ('UPDATE {smartlist_users} SET lstatus = %d WHERE uid = %d AND lid = %d', $lstatus, $uid, $lid);
  }
  
  return $result;
}

/**
 * Create a new subscription by sending request email to Smartlist.
 */
function _smartlist_manager_subscribe($uid, $lid, $mail_subscribe, $digest) {
  $params = array();
  $query = 'SELECT * FROM {smartlist_lists} WHERE lid = %d';
  $result = db_query($query, $lid);
  $list = db_fetch_array($result);
  $commandaddress = str_replace("@", "-request@", $list['name']);
  $adminaddress = $list['admin'];
  $password = $list['password'];
  $bcc = $list['bcc'];

  $params['command'] = 'subscribe ' . $mail_subscribe;
  $params['x_command'] = $adminaddress . ' ' . $password . ' subscribe '. trim($mail_subscribe);


  // If the adminaddress was given, use BCC
  if ($bcc != '') {
    $params['bcc'] = $bcc;
  }
  $mailsuccess = drupal_mail('smartlist_manager', 'subscribe', $commandaddress, language_default(), $params, $adminaddress);
  $query = "UPDATE {smartlist_users} SET lmail = '%s' WHERE uid = %d AND lid = %d";
  if ($mailsuccess['result'] && db_query($query, $mail_subscribe, $uid, $lid)) {
    watchdog('smartlist man', 'New subscription to list %lid for user %uid completed successfully.', array('%lid' => $lid, '%uid' => $uid), WATCHDOG_NOTICE);
  }
  else {
    watchdog('smartlist man', 'New subscription to list %lid for user %uid failed : !error',  array('!error' => $mailsuccess['result'], '%lid' => $lid, '%uid' => $uid), WATCHDOG_ERROR);
  }

  //var_dump($mailsuccess);
  return $mailsuccess['result'];
}

/**
 * Update settings for a subscription by sending request email to Smartlist.
 */
function _smartlist_manager_setdelivery($uid, $lid, $mail_subscribe, $command, $x_command) {

  $query = 'SELECT * FROM {smartlist_lists} WHERE lid = %d';
  $result = db_query($query, $lid);
  $list = db_fetch_array($result);
  $commandaddress = str_replace("@", "-request@", $list['name']);
  $adminaddress = $list['admin'];
  $password = $list['password'];
  $bcc = $list['bcc'];

  $params['command'] = $command;
  $params['x_command'] = $adminaddress . ' ' . $password . ' ' . $x_command;

  if ($bcc != '') {
    $params['bcc'] = $bcc;
  }

  watchdog('smartlist man', 'Mail command sent to Smartlist: %commandaddress. Command: %command. X-Command: %x_command', array('%commandaddress' => $commandaddress, '%command' => $command, '%x_command' => $x_command), WATCHDOG_NOTICE);
  $mailsuccess = drupal_mail('smartlist_manager', 'notify', $commandaddress, language_default(), $params, $mail_from);

  //var_dump($mailsuccess);
  return $mailsuccess['result'];
}

function smartlist_manager_mail($key, &$message, $params) {
  if ($params['bcc']) {
    $message['headers'] = array_merge($message['headers'], array('bcc' => $params['bcc']));
    //    $message['headers']['bcc'] = $params['bcc'];
  }
  //US Smartlist requires the x-command header
  if ($params['x_command']) {
    $message['headers'] = array_merge($message['headers'], array('X-Command' => $params['x_command']));
    //    $message['headers']['X-Command'] = $params['x_command'];
  }

  //US test from header
  $message['headers'] = array_merge($message['headers'], array('From' => 'info@calheat.com'));
  
  
  $message['subject'] = $params['command'];
  $message['body'][] = $params['command'];
}


/**
 * Check if a user can access the list subscription form.
 */
function _smartlist_manager_access($account) {
  global $user;
  return $account && $account->uid &&
  (
  // Always let users view their own profile.
  ($user->uid == $account->uid && user_access('access smartlist_manager')) ||
  // Smartlist administrators are allowed to change subscriptions.
  user_access('administer smartlist_manager')
  );
}

/**
 * Verifies if a user is entitled to view an object or not.
 *
 * Permission check needs to be done outside.
 * 
 * @param $usr The user object you want to check for. If NULL check for the current user.
 * @param $roles String a string with roles(separated by semicolon) 
 * @return true or false. 
 * 
 */
function _smartlist_manager_roles_access($usr, $roles) {

  $role_view = false;
  
  $_roles = explode(';', $roles);
  
  
  if (is_null($usr)) {
    if (user_is_anonymous()) {
      $usr_roles = array(1); //=anonymous
    }
    else {
      global $user;
      $usr_roles = array_keys($user->roles);
    }
  }
  else {
    //get roles from $usr allowed to access the rsvp.
    $usr_roles = array_keys($usr->roles);
  }

  if (count(array_intersect($_roles, $usr_roles)) > 0) {
    $role_view = true;
  }
  
  return $role_view;
}

